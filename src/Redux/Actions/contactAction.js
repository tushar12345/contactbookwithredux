import {
  CREATE_CONTACT,
  GET_CONTACT,
  UPDATE_CONTACT,
  DELETE_CONTACT,
  SELECTALL_CONTACT,
  CLEARALL_CONTACT,
  DELETESELECTED_CONTACT,
} from "../actionType";

// action
export const addContact = (contact) => {
  return {
    type: CREATE_CONTACT,
    payload: contact,
  };
};

//get contact
export const getContact = (id) => ({
  type: GET_CONTACT,
  payload: id,
});

//update contact
export const updateContact = (contact) => ({
  type: UPDATE_CONTACT,
  payload: contact,
});

//delete contact
export const deleteContact = (id) => ({
  type: DELETE_CONTACT,
  payload: id,
});

//selectall contact
export const selectAllContact = (id) => ({
  type: SELECTALL_CONTACT,
  payload: id,
});

//clear all contacts
export const clearAllContact = () => ({
  type: CLEARALL_CONTACT,
});

//delete all contacts
export const deleteAllContact = () => ({
  type: DELETESELECTED_CONTACT,
});
