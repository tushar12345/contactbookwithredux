import "./Styles/App.scss";

import { Navbar } from "./Components/Navbar/Navbar";
import { Contacts } from "./Components/contacts/Contacts";
import { Provider } from "react-redux";
import store from "./Redux/store";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AddContact } from "./Components/contacts/AddContact";
import { EditContact } from "./Components/contacts/EditContact";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navbar />
          <div className="container">
            <div className="py-3">
              <Switch>
                <Route exact path="/" component={Contacts} />
                <Route exact path="/contacts/add" component={AddContact} />
                <Route
                  exact
                  path="/contacts/edit/:id"
                  component={EditContact}
                />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;

//commit date 04/11/2020
