import React from "react";
import Avatar from "react-avatar";
import { Link } from "react-router-dom";
import { deleteContact } from "../../Redux/Actions/contactAction";
import { useDispatch } from "react-redux";

export const Contact = ({ contact, selectAll }) => {
  const dispatch = useDispatch();
  const { name, phone, email, id } = contact;
  return (
    <tr>
      <td>
        <div className="form-check">
          <input
            className="form-check-input"
            type="checkbox"
            id="selectall"
            checked={selectAll}
          />
          <label className="form-check-label" htmlfor="selectall"></label>
        </div>
      </td>
      <td>
        <Avatar className="mr-2" size="45" round={true} name={name} />
        {name}
      </td>
      <td>{phone}</td>
      <td>{email}</td>
      <td className="actions">
        <Link to={`/contacts/edit/${id}`} className="material-icons mr-2 ">
          edit
        </Link>
        <span
          className="material-icons text-danger"
          onClick={() => dispatch(deleteContact(id))}
        >
          delete
        </span>
      </td>
    </tr>
  );
};
