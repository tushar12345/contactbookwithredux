import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Contact } from "./Contact";
import {
  selectAllContact,
  clearAllContact,
  deleteAllContact,
} from "../../Redux/Actions/contactAction";

export const Contacts = () => {
  const [selectAll, setSelectAll] = useState(false);
  const dispatch = useDispatch();
  const contacts = useSelector((state) => state.contact.contacts);
  const selectedContacts = useSelector(
    (state) => state.contact.selectAllContacts
  );

  useEffect(() => {
    if (selectAll) {
      dispatch(selectAllContact(contacts.map((contact) => contact.id)));
    } else {
      dispatch(clearAllContact());
    }
    return () => {};
  }, [selectAll]);
  return (
    <div>
      {selectedContacts.length > 0 ? (
        <button
          className="btn btn-danger my-3"
          onClick={() => dispatch(deleteAllContact())}
        >
          Delete All
        </button>
      ) : null}
      <table className="table shadow">
        <thead className="bg-danger text-white">
          <tr>
            <th>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="selectall"
                  value={selectAll}
                  onClick={() => setSelectAll(!selectAll)}
                />
                <label className="form-check-label" htmlfor="selectall"></label>
              </div>
            </th>
            <th>Name</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {contacts.map((contact) => (
            <Contact contact={contact} key={contact.id} selectAll={selectAll} />
          ))}
        </tbody>
      </table>
    </div>
  );
};
